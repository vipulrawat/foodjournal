var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

//Under testing
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

//Custom imports
var router = require('./routes/routes.js');

var app = express();


app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../client'));
app.use(express.static(path.join(__dirname, '../client')));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

app.use(cookieParser());
//session config
app.use(session({
    secret: 'find some hobby',
    saveUninitialized: true,
    resave: true
}));
//passport config
app.use(passport.initialize());
app.use(passport.session());
//connect-flash middleware
app.use(flash());
//Some global variables for flash
app.use((req,res,next)=>{
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
  
    next();
});

app.use('/', router);

module.exports = app;