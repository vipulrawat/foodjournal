var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

//under testing
var { Pool, Client } = require('pg');
var client = new Client({
  user: 'xpyrguww',
  host: 'stampy.db.elephantsql.com',
  database: 'xpyrguww',
  password: 'SS2w4HoWZb9LiByX2r3yEZDbj9P0PWr8',
  port: 5432,
});
client.connect();
//
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

router.get('/login',(req,res)=>{
  res.render('login');
});
router.get('/register',(req,res)=>{
  res.render('register');
});
router.post('/register',(req,res)=>{
  var username = req.body.name;
  var password = req.body.password;
  let queryString = 'INSERT INTO users(username,password) VALUES($1,$2)';
  let params = [username, password];
  client.query(queryString, params)
    .then(res => console.log(res.rows[0]))
    .catch(e => console.error(e.stack))
});

passport.use('local', new LocalStrategy({
  passReqToCallback : true,
  usernameField: 'username'
},
function(req, username, password, done){
console.log('called local');

 console.log('called local - pg');

 var user = {};

   var query = client.query("SELECT * FROM users WHERE username = $1", [username],(err,res)=>{
     console.log('User obj', res.rows[0]);
     console.log('Password', password)
     user = res.rows[0];
     if(password == user.password){
       console.log('match!')
       done(null, user);
     } else {
       done(null, false, { message: 'Incorrect username and password.' });
     }
   });



   // After all data is returned, close connection and return results




}));

passport.serializeUser(function(user,done){
done(null,user.id);
});
passport.deserializeUser(function(id, done) {
   console.log('called deserializeUser');

     var user = {};
     console.log('called deserializeUser - pg');
       var query = client.query("SELECT * FROM users WHERE id = $1", [id],(err,res)=>{
         console.log('User row', res.rows[0]);
         user = res.rows[0];
         done(null, user);
       });



       // After all data is returned, close connection and return results




});




router.post('/login',
     passport.authenticate('local',{successRedirect:'/',failureRedirect:'/login',failureFlash: true}),
     function(req,res){
         res.redirect('/');
});

router.get('/logout',function(req,res){
    req.logout();
    req.flash('success_msg','You are logged out');
    res.redirect('/login');
})
router.get('/',ensureAuthenticated,function(req,res){
  res.render('index');
});

function ensureAuthenticated(req,res,next) {
  if(req.isAuthenticated()){
    return next();
  }else{
    req.flash('error_msg','You are not logged in');
    res.redirect('/login');
  }
}
//
/**
router.get('/', (req, res) => {
  res.render('index');
});
*/
router.route('/insert').post((req, res) => {
  let fN = req.body.foodN;
  let qty = req.body.qty;
  fN = fN.toString(fN);
  qty = parseInt(qty);
  let queryString = 'INSERT INTO userinput(foodname,quantity) VALUES($1,$2)';
  let params = [fN, qty];
  client.query(queryString, params)
    .then(res => console.log(res.rows[0]))
    .catch(e => console.error(e.stack))
});

router.get('/getAll', (req, res) => {
  let queryString = 'SELECT userinput.foodname,userinput.quantity,calories.caloriesvalue FROM userinput INNER JOIN calories ON (userinput.foodname = calories.foodname)';
  let qs = 'Select * from userinput';
  client.query(queryString, function (err, rs) {
    if (err) {
      console.log(err.stack)
    } else {
      let ans = JSON.stringify(rs.rows);
      console.log(ans);
      res.send(ans);
    }
  });
});

module.exports = router;