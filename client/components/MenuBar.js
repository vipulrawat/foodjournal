import React from 'react';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
const { Header, Content, Footer, Sider } = Layout;
import 'antd/dist/antd.css';
import Axios from 'axios';
const SubMenu = Menu.SubMenu;

class MenuBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false
    }
    this.logout = this.logout.bind(this);
  }
  logout(){
    axios.post('/logout')
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
  }
  render() {
    return (
      <Layout style={{ minHeight: '100vh', maxWidth: '50px' }}>
        <Sider
          collapsed={this.state.collapsed}
        >
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            <Menu.Item key="1">
              <Icon type="dashboard" />
              <span>Dashboard</span>
            </Menu.Item>
            <Menu.Item key="2" disabled="true">
              <Icon type="pie-chart" />
              <span>Visualize</span>
            </Menu.Item>
            <Menu.Item key="3" disabled="true">
              <Icon type="notification" />
              <span>Notifications</span>
            </Menu.Item>
            <Menu.Item key="4" disabled="true">
              <Icon type="file" />
              <span>Add food</span>
            </Menu.Item>
            <Menu.Item key="5">
              <Icon type="setting" spin="true" />
              <span>Settings</span>
            </Menu.Item>
            <Menu.Item key="6" onClick={()=>this.logout()}>
              <Icon type="user" spin="false" />
              <span>Logout</span>
            </Menu.Item>
          </Menu>
        </Sider>
      </Layout>
    );
  }
}

export default MenuBar;