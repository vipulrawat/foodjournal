import React from 'react';
import { Modal, Button,Select,InputNumber } from 'antd';
import axios from 'axios';
var querystring  = require('querystring');
const Option = Select.Option;
class ModalSample extends React.Component {
  
  constructor(props){
      super(props);
      this.state={
        visible: false,
        confirmLoading: false,
        food: '',
        quantity: 1,
        messageFromServer:''
      }
      this.handleSelectChange = this.handleSelectChange.bind(this);
      this.handleInputChange = this.handleInputChange.bind(this);
  }
  showModal(){
    this.setState({
        visible: true,
      });
  }

  handleOk() {
    this.setState({
      confirmLoading: true,
      
    });
    //axios post request
    axios.post('/insert',
      querystring.stringify({
          foodN: this.state.food,
          qty: this.state.quantity,
      }), {
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }
      }).then(function (response) {
          this.setState({
              messageFromServer: response.data
          });
    });
    //
    console.log(`Values from handleOK:`+this.state.food+`::`+this.state.quantity+`:::`+this.state.messageFromServer);
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  }
  handleCancel (){
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  }
  handleSelectChange(e){
    this.setState({food: e});
    console.log(e)
  }
  handleInputChange(e){
    this.setState({quantity: e});
    console.log(e);
  }
  render() {
    const { visible, confirmLoading} = this.state;
    return (
      <div>
        <Button type="primary" icon="plus-circle-o" onClick={()=>this.showModal()}>Add</Button>
        <Modal title="Add Food Item"
          visible={visible}
          onOk={()=>this.handleOk()}
          confirmLoading={confirmLoading}
          onCancel={()=>this.handleCancel()}
        >
        <div>
        <label htmlFor="food">Select Food:</label>
          <Select defaultValue={this.state.food} style={{ width: 120 }} id="foods" name="food" onChange={this.handleSelectChange}>
            <Option value="egg" id="egg">Egg</Option>
            <Option value="bread" id="bread" >Bread</Option>
            <Option value="caviar" id="caviar">Caviar</Option>
            <Option value="Regular pizza" id="Regular pizza">Regular pizza</Option>
          </Select>
          Quantity:
         <InputNumber min={1} max={50} defaultValue={1} name="quantity" id="quantity" onChange={this.handleInputChange}/>
          </div>
        </Modal>
      </div>
    );
  }
}

export default ModalSample;