import React from 'react';
import { Table, Icon, Divider, Card, Button } from 'antd';
import axios from 'axios';
import '../css/App.css';

var columns = [{
  title: 'Food Item',
  dataIndex: 'food',
  key: 'food',
}, {
  title: 'Quantity',
  dataIndex: 'quantity',
  key: 'quantity',
}, {
  title: 'Calories',
  dataIndex: 'calories',
  key: 'calories',
}];

var data = [];

class TableSample extends React.Component {

  constructor() {
    super();
    this.state = {
      initSum: 0,
      arrayObjects: data,
      arr: []
    }
    this.getData = this.getData.bind(this);
  }
  /*
    checkSum(){
     let sum =0; 
     this.state.arrayObjects.forEach(element => {
       sum+=element.calories;
     });
      console.log(`Total Calories:`+sum);
      this.setState({initSum: sum});
    }
    */
  componentDidMount() {
    this.getData(this);
    console.log('Component is mounted');

  }
  componentWillReceiveProps(nextProps) {
    this.getData(this);
  }
  /*
  getSum(){
    let sum =0; 
    this.state.arr.forEach(element => {     
      sum += element.caloriesvalue*element.quantity
    });
    console.log(`Total Calories:`+sum);
    this.setState({initSum: sum});
  }
  */
  getData(ev) {
    axios.get('/getAll')
      .then(function (response) {
        console.log('INSIDE GETDATA:' + JSON.stringify(response.data));
        ev.setState({ arr: response.data });

      });
  }
  initData() {
    var temp = 0;
    var i = 0;
    this.state.arr.map(function (exp) {
      i++;

      let newObj = {
        key: i,
        food: exp.foodname,
        quantity: exp.quantity,
        calories: exp.caloriesvalue
      }
      data.push(newObj);
      temp += exp.quantity * exp.caloriesvalue;

    })
    return temp;
    console.log("Total Calories:" + temp);
  }

  render() {

    var { initSum } = this.state;
    var sum = this.initData();
    return (
      <div>
        <Table columns={columns} dataSource={data} />
        <Card title="Total Calories intake" style={{ width: '100%', fontWeight: 'bold' }}>
          <h3>Your total calories intake for today is : <span id="calories">{sum}</span></h3>
        </Card>

      </div>
    );
  }
}
export default TableSample;