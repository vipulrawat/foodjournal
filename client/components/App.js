import React, { Component } from 'react';
import { Row, Col } from 'antd';
//style imports
import '../css/App.css';
//custom imports
import MenuBar from './MenuBar';
import TableSample from './TableSample';
import AddFood from './AddFood';

class App extends Component {
  render() {
    return (
      <div>
        <Row>
        <Col span={4}><MenuBar/></Col>
        <Col span={20}>
          <AddFood/>
          <h1>Today</h1>
          <TableSample/>
        </Col>
     
        </Row>
      </div>
    );
  }
}

export default App;
 